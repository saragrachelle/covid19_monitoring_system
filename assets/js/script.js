let ans1, ans2, ans3, ans4, ans5, ans6;
let tmpnon, tmp;
let free = true;
let pui = false;

const tempnon = () => {
  swal("Do you provide direct care for Covid-19 Patient? (yes/no)", {
    content: "input",
  }).then((value) => {
    ans1 = value;
    ans1 = ans1.toLowerCase();
    if (ans1 === "yes") free = false;

    swal(
      "Are you working together or staying in the same close enviroment of a Covid-19 patient? (yes/no)",
      {
        content: "input",
      }
    ).then((value) => {
      ans2 = value;
      ans2 = ans2.toLowerCase();
      if (ans2 === "yes") free = false;
      swal(
        "Did you travel with Covid-19 Patient in any kind of conveyance? (yes/no)",
        {
          content: "input",
        }
      ).then((value) => {
        ans3 = value;
        ans3 = ans3.toLowerCase();
        if (ans3 === "yes") free = false;
        swal(
          "Are you living in the same household as a Covid-19 patient within a 14-day period? (yes/no)",
          {
            content: "input",
          }
        ).then((value) => {
          ans4 = value;
          ans4 = ans4.toLowerCase();
          if (ans4 === "yes") free = false;
          checkNon(free);
        });
      });
    });
  });
};

const checkNon = (checking) => {
  if (checking === false) {
    temp();
  } else {
    swal("You are safe", "Always wear face mask", "info");
  }
};

const temp = () => {
  swal("Do you have colds?", {
    content: "input",
  }).then((value) => {
    ans5 = value;
    ans5 = ans5.toLowerCase();
    console.log(ans5);
    if (ans5 === "yes") {
      pui = true;
    }
    swal("Do you have cough?", {
      content: "input",
    }).then((value) => {
      ans6 = value;
      ans6 = ans6.toLowerCase();
      console.log(ans6);
      if (ans5 === "yes") {
        pui = true;
      }
      swal("Enter temperature", {
        content: "input",
      }).then((value) => {
        tmp = value;
        console.log(tmp);
        if (tmp >= 38.0) {
          pui = true;
        }
        checkTravel(pui);
      });
    });
  });
};

const checkTravel = (pui) => {
  if (pui === true) {
    swal(
      "YOU ARE PERSON UNDER INVESTIGATION",
      "Call a medical expert and get yourself isolated",
      "error"
    );
  } else {
    swal("YOU ARE PERSON UNDER MONITOR", "Do a home quarantine", "warning");
  }
};
